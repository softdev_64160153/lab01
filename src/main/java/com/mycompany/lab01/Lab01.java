/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.lab01;

import java.util.Scanner;

/**
 *
 * @author mookda
 */
public class Lab01 {

    private static char[][] board;
    private static char player;

    public Lab01() {
        System.out.println("Welcome to OX");
        System.out.println("!!!Game Start!!!");

        board = new char[3][3];
        player = 'X';

        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                board[row][col] = ' ';
            }
        }
    }

    public static void drawGrid() {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                System.out.print("|" + board[row][col] + "|");
            }
            System.out.println();
        }
    }

    public boolean boardFull() {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                if (board[row][col] == ' ') {
                    return false;
                }
            }
        }
        return true;

    }

    private boolean checkWinner() {
        //แนวนอน
        for (int i = 0; i < 3; i++) {
            if (board[i][0] == player && board[i][1] == player && board[i][2] == player) {
                return true;
            }
        }

        //แนวตั้ง
        for (int j = 0; j < 3; j++) {
            if (board[0][j] == player && board[1][j] == player && board[2][j] == player) {
                return true;
            }
        }

        //ทแยง
        if (board[0][0] == player && board[1][1] == player && board[2][2] == player) {
            return true;
        }
        if (board[0][2] == player && board[1][1] == player && board[2][0] == player) {
            return true;
        }

        return false;

    }

    public void playGame() {
        Scanner sc = new Scanner(System.in);

        while (true) {
            drawGrid();
            System.out.println("- Player " + player + " Turn -");
            System.out.print("Enter your Move(rowcol): ");

            String input = sc.nextLine();

            if (input.matches("\\d\\d")) {

                int row = Character.getNumericValue(input.charAt(0));
                int col = Character.getNumericValue(input.charAt(1));

                if (row >= 0 && row < 3 && col >= 0 && col < 3 && board[row][col] == ' ') {
                    board[row][col] = player;

                    if (checkWinner()) {
                        drawGrid();
                        System.out.println("Player " + player + " wins!");
                        break;
                    }

                    if (boardFull()) {
                        drawGrid();
                        System.out.println("It's a tie!");
                        break;
                    }

                    player = (player == 'X') ? 'O' : 'X';
                } else {
                    System.out.println("Invalid move, Please try again.");
                }

            } else {
                System.out.println("Invalid input format, Please enter the position as rowcol.");
            }
        }

    }

    public static void main(String[] args) {
        Lab01 ox = new Lab01();
        ox.playGame();

    }
}
